<?php

namespace App\Http\Controllers\Api;

use App\Events\PostCreated;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index(){
        $posts = Post::with('user')->latest()->get()->map(function($post) {
            $post['is_liked_by_user'] = $post->isLikedByUser(Auth::user());
            return $post;
        });

        return response()->json(['posts' => PostResource::collection($posts)]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'body' => 'required',
            'image_path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $uploadPath = 'public/images';
        $imageFile = $request->file('image_path');
        $imageName = time()."_".$imageFile->getClientOriginalName();
        //store to public path
        //$imageFile->move($uploadPath,$imageName);
        //if you want to store to storage path
        $imageFile->storeAs($uploadPath, $imageName);

        $post = $request->user()->posts()->create([
            'body' => $request->body,
            'image_path' => $imageName
        ]);

        $postWithUser = $post->with('user')->find($post->id);

        event(new PostCreated($postWithUser));

        return response()->json(['post' => $postWithUser]);
    }
}
