<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    public function show(Request $request)
    {
        return response()->json([
            'user' => $request->user(),
        ], 200);
    }

    public function updateAvatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        // if user has already an avatar, delete it
        if ($request->user()->avatar_url && Storage::exists($request->user()->avatar_url)) {
            Storage::delete($request->user()->avatar_url);
        }

        $uploadPath = 'public/avatars';
        $avatarFile = $request->file('avatar');
        $user = $request->user();
        $avatarName = $user->id . '_avatar_' . time().$avatarFile->getClientOriginalName();
        // Store the avatar in the 'public/avatars' directory
        $avatarFile->storeAs($uploadPath, $avatarName);
        // Update the user's avatar URL in the database using the update method
        $user->update(['avatar_url' => $avatarName]);
        return response()->json([
            'user' => $request->user()
        ], 200);
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'bio' => 'nullable|string|max:255|'
        ]);

        $user = $request->user();
        $user->update([
            'name' => $request->name,
            'bio' => $request->bio
        ]);

        return response()->json([
            'user' => $user
        ], 200);
    }
}
