<?php

namespace App\Http\Controllers\api;

use App\Events\CommentPosted;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostCommentController extends Controller
{
    public function index(Post $post)
    {
        $comments = $post->comments()->with(['user', 'replies', 'replies.user'])->get();
        return response()->json(['comments' => $comments]);
    }

    public function store(Request $request, Post $post)
    {
        $request->validate([
            'body' => 'required'
        ]);
        
        $comment = $post->comments()->create([
            'body' => $request->body,
            'user_id' => auth()->user()->id
        ]);

        $comment->load('user');
        event(new CommentPosted($comment));

        return response()->json(['comment' => $comment], 201);
    }
}
