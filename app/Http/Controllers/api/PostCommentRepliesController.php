<?php

namespace App\Http\Controllers\api;

use App\Events\ReplyPosted;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class PostCommentRepliesController extends Controller
{
    public function store(Request $request, Comment $comment)
    {
        $request->validate([
            'body' => ['required']
        ]);

        $reply = Comment::create([
            'body' => $request->body,
            'user_id' => auth()->user()->id,
            'parent_id' => $comment->id,
            'post_id' => $comment->post->id
        ]);

        $reply->load('user');
        event(new ReplyPosted($reply));

        return response()->json(['reply' => $reply], 201);
    }
}
