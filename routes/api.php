<?php

use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\LikeController;
use App\Http\Controllers\api\PostCommentController;
use App\Http\Controllers\api\PostCommentRepliesController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\api\ProfileController;
use App\Http\Controllers\api\SettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function() {
    Route::post('/logout', [AuthController::class, 'logout']);
    //Post
    Route::get('posts', [PostController::class, 'index']);
    Route::post('posts', [PostController::class, 'store']);
    //Like
    Route::post('posts/{post}/like', LikeController::class);
    //Comment
    Route::get('posts/{post}/comment', [PostCommentController::class, 'index']);
    Route::post('posts/{post}/comment', [PostCommentController::class, 'store']);
    //Reply
    Route::post('comment/{comment}/reply', [PostCommentRepliesController::class, 'store']);
    //Profile
    Route::get('/users/{user}', [ProfileController::class, 'showProfile']);
    Route::get('/users/{user}/posts', [ProfileController::class, 'userPosts']);
    //Settings
    Route::get('/settings', [SettingController::class, 'show']);
    Route::post('/settings', [SettingController::class, 'updateProfile']);
    Route::post('/settings/avatar', [SettingController::class, 'updateAvatar']);
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);